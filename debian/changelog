rifiuti2 (0.7.0-5) unstable; urgency=medium

  * Team upload.
  * debian/control: bumped Standards-Version to 4.7.0.

  [ Santiago Vila ]
  * Update gettext patch for 0.23.x (Closes: #1092235)

 -- Alexandre Detiste <tchet@debian.org>  Mon, 13 Jan 2025 11:09:31 +0100

rifiuti2 (0.7.0-4) unstable; urgency=medium

  * debian/control: bumped Standards-Version to 4.6.2.
  * debian/watch: updated the search rule.

 -- Giovani Augusto Ferreira <giovani@debian.org>  Sun, 28 Apr 2024 22:25:36 -0300

rifiuti2 (0.7.0-3) unstable; urgency=medium

  * Bumped standards version to 4.5.1, no changes needed.
  * debian/patches/001: Add temporary patch to fix FTBFS with
    gettext 0.21. (Closes: #978335)

 -- Giovani Augusto Ferreira <giovani@debian.org>  Sun, 28 Apr 2024 22:06:47 -0300

rifiuti2 (0.7.0-2) unstable; urgency=medium

  [ Samuel Henrique ]
  * Add salsa-ci.yml
  * Configure git-buildpackage for Debian

  [ Giovani Augusto Ferreira ]
  * Bumped DH level to 13.
  * Bumped standards version to 4.5.0, no changes needed.
  * debian/control:
      - Added 'Rules-Requires-Root: no' to source stanza.
  * debian/copyright: updated packaging years.
  * debian/tests/control: add superficial restriction. (closes: #969863)
  * Set upstream metadata fields:
      - Bug-Database, Bug-Submit, Repository, Repository-Browse.
  * Run wrap-and-sort.

 -- Giovani Augusto Ferreira <giovani@debian.org>  Thu, 10 Sep 2020 17:51:29 -0300

rifiuti2 (0.7.0-1) unstable; urgency=medium

  * New upstream version 0.7.0
  * Bumped DH level to 12.
  * Bumped Standards-Version to 4.4.0
  * debian/clean: updated listed files.
  * debian/copyright: removed unused references.
  * debian/patches/*: removed all patches.
  * debian/rules: removed an override.
  * debian/tests/*: created to provide trivial tests.

 -- Giovani Augusto Ferreira <giovani@debian.org>  Sun, 28 Jul 2019 21:00:54 -0300

rifiuti2 (0.6.1-6) unstable; urgency=medium

  [ Raphaël Hertzog ]
  * Update team maintainer address to Debian Security Tools
    <team+pkg-security@tracker.debian.org>
  * Update Vcs-Git and Vcs-Browser for the move to salsa.debian.org

  [ Giovani Augusto Ferreira ]
  * Dropped autoreconf depends
  * Bumped Standards-Version to 4.2.1
  * Bumped DH level to 11
  * Updated my email address
  * debian/copyright: updated URI and packaging copyright years.
  * Added upstream metadata file

 -- Giovani Augusto Ferreira <giovani@debian.org>  Wed, 29 Aug 2018 16:08:19 -0300

rifiuti2 (0.6.1-5) unstable; urgency=medium

  * Update DH level to 10.
  * debian/compat: updated to 10.
  * debian/rules: removed the '--with autoreconf' because it is default
    in DH 10.

 -- Giovani Augusto Ferreira <giovani@riseup.net>  Sat, 17 Dec 2016 00:25:59 -0200

rifiuti2 (0.6.1-4) unstable; urgency=medium

  * debian/control:
      - Bumped Standards-Version to 3.9.8.
  * debian/patches:
      - Added the '.patch' extension to all patches.

 -- Giovani Augusto Ferreira <giovani@riseup.net>  Sun, 25 Sep 2016 00:30:11 -0300

rifiuti2 (0.6.1-3) unstable; urgency=medium

  * debian/control:
      - Changed from cgit to git in Vcs-Browser field.
  * debian/watch: bumped to version 4.

 -- Giovani Augusto Ferreira <giovani@riseup.net>  Wed, 23 Mar 2016 20:54:11 -0300

rifiuti2 (0.6.1-2) unstable; urgency=medium

  * New co-maintainer. Thanks Eriberto Mota for great work in the last upload.
  * debian/control:
      - Bumped Standards-Version to 3.9.7.
      - Updated Vcs-* fields.
  * debian/copyright: added my name at debian/* block.

 -- Giovani Augusto Ferreira <giovani@riseup.net>  Sat, 20 Feb 2016 23:04:38 -0200

rifiuti2 (0.6.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * New upstream homepage.
  * New co-maintainer.
  * Added a directory as example. Consequently:
      - debian/examples: added to install the example.
      - debian/RECYCLE: added. This is the example.
      - debian/source/include-binaries: added to allow the files used as
        example.
  * Migrations:
      - debian/copyright to 1.0 format.
      - debian/rules to new (reduced) format.
      - DebSrc to 3.0.
      - DH level to 9.
      - Using autoreconf now.
  * debian/clean: added to remove files created by upstream.
  * debian/control:
      - Bumped Standards-Version to 3.9.6.
      - Re-written the short and long descriptions.
      - Updated the Vcs-* fields.
  * debian/copyright: updated all information.
  * debian/patches/fix-no-format-arguments: added to fix a FTBFS caused by no
      format arguments.
  * debian/rifiuti2.docs:
      - Renamed to docs.
      - Added README.md and NEWS.md.
      - Removed non existent NEWS, THANKS and TODO.
  * debian/source.lintian-overrides: removed. Using 'Team upload' now.
  * debian/watch: updated.

 -- Joao Eriberto Mota Filho <eriberto@debian.org>  Sun, 26 Jul 2015 19:15:32 -0300

rifiuti2 (0.5.1-3) unstable; urgency=low

  * Updating package to standards version 3.8.2.
  * Adding lintian overrides.

 -- Daniel Baumann <daniel@debian.org>  Thu, 09 Jul 2009 12:32:22 +0200

rifiuti2 (0.5.1-2) unstable; urgency=low

  * Updating year in copyright file.
  * Adding misc-depends in control.
  * Updating standards to 3.8.1.

 -- Daniel Baumann <daniel@debian.org>  Thu, 28 May 2009 22:21:39 +0200

rifiuti2 (0.5.1-1) unstable; urgency=low

  [ Anthony Wong ]
  * Initial release (Closes: #506952).

  [ Daniel Baumann ]
  * Adding vcs fields in control.
  * Rewrapping and improving wording of package long-description in control.
  * Rewriting copyright file in machine-interpretable format.
  * Removing AUTHORS from docs, no new information over than what is already in
    debian/copyright.
  * Removing README from docs, no new information other than what is already in
    the package long-description.
  * Prefixing debhelper files with package name.
  * Rewriting rules file.
  * Removing useless empty line at the end of changelog.
  * Adding wnpp bug number to changelog.
  * Adding myself to copyright.
  * Removing unnecessary version in libglib2.0-dev build-depends.
  * Removing upstream symlink for rifiuti-vista manpage.

 -- Daniel Baumann <daniel@debian.org>  Tue, 20 Jan 2009 21:28:17 +0100
